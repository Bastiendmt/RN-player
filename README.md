# rn-appstud-test
Projet réalisé dans le cadre d'un entretien d'embauche

## Instalation

`git clone https://gitlab.com/bastienxs/RN-player.git`

`cd RN-player`

`npm install`

`expo start`

## Captures d'écran

### Accueil
![](images/home.png)


### Playlist
![](images/playlist.png)